const MAX_LAST_PING = 300;
const offset = window.outerHeight - window.innerHeight;
const canvas = document.querySelector('canvas');

function resize() {
  if (!canvas) {
    return;
  }

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}

const windowId = crypto.randomUUID();
document.title = `Window Demo ${windowId}`;

resize();

window.addEventListener('resize', resize);

const ctx = canvas?.getContext('2d');

type Box = {
  id: string;
  windowId: string;
  x: number;
  y: number;
  vy: number;
  vx: number;
  ttl: number;
  lastPing: number;
};

type Bar = {
  windowId: string;
  x: number;
  y: number;
  width: number;
  lastPing: number;
};

const boxes: Box[] = [];
const alienBoxes: Box[] = [];
const windowBars: Record<string, Bar> = {};

const channel = new BroadcastChannel("comms");

channel.onmessage = (event) => {
  const [ kind, payload ] = event.data;

  switch (kind) {
    case "box": {
      alienBoxes.push(payload);
      break;
    }
    case "update": {
      const box = alienBoxes.find((box) => box.id === payload.id);
      if (box) {
        if (payload.y >= window.screenY + window.innerHeight && payload.x > window.screenX && payload.x <= window.screenX + window.innerWidth) {
          channel.postMessage(["stop", { id: box.id, x: box.x, y: box.y }]);
          return;
        }
        box.x = payload.x;
        box.y = payload.y;
      } else {
        alienBoxes.push({
          ...payload,
          lastPing: Date.now(),
        });
      }
      break;
    }
    case "stop": {
      const box = boxes.find((box) => box.id === payload.id);
      if (box) {
        box.vy = -10;
        box.x = payload.x;
        box.y = payload.y;
      }
      break;
    }
    case "delete": {
      const index = alienBoxes.findIndex((box) => box.id === payload.id);
      if (index > -1) {
        alienBoxes.splice(index, 1);
      }
      break;
    }
    case "ping": {
      const now = Date.now();
      alienBoxes.forEach((box) => {
        if (box.windowId === payload.windowId) {
          box.lastPing = now;
        }
      });

      windowBars[payload.windowId] = {
        windowId: payload.windowId,
        x: payload.x,
        y: payload.y,
        width: payload.width,
        lastPing: Date.now(),
      };
      break;
    }
  }
};

document.addEventListener('click', (e) => {
  const box: Box = {
    id: crypto.randomUUID(),
    windowId,
    x: e.screenX,
    y: e.screenY - offset,
    vy: 0,
    vx: 0,
    ttl: Date.now() + 10000,
    lastPing: Date.now(),
  };
  boxes.push(box);
  channel.postMessage(["box", box]);
});

function physics(delta, now) {
  boxes.forEach((box, i) => {
    if (box.ttl < now) {
      boxes.splice(i, 1);
      channel.postMessage(["delete", { id: box.id }]);
      return;
    }
    box.vy += 10 * delta / 1000;

    box.y += box.vy;

    channel.postMessage(["update", { id: box.id, x: box.x, y: box.y }]);
  });

  channel.postMessage(["ping", { windowId, x: window.screenX, y: window.screenY + window.innerHeight, width: window.innerWidth }]);

  alienBoxes.forEach((box, i) => {
    if (now - box.lastPing > MAX_LAST_PING) {
      alienBoxes.splice(i, 1);
    }
  });
  Object.keys(windowBars).forEach((bar) => {
    if (now - windowBars[bar].lastPing > MAX_LAST_PING) {
      delete windowBars[bar];
    }
  });
}

let lastUpdate = Date.now();
setInterval(() => {
  const now = Date.now();
  physics(now - lastUpdate, now);
  lastUpdate = now;
}, 1000 / 120);

function draw() {
  requestAnimationFrame(draw);

  if (!ctx) {
    return;
  }

  ctx.fillStyle = '#666';
  ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);

  ctx.save();
  ctx.translate(-window.screenX, -window.screenY);

  ctx.fillStyle = 'red';
  boxes.forEach((box) => {
    ctx.fillRect(box.x - 16, box.y - 16, 32, 32);
  });
  ctx.fillStyle = 'orange';
  alienBoxes.forEach((box) => {
    ctx.fillRect(box.x - 16, box.y - 16, 32, 32);
  });

  ctx.fillStyle = 'black';
  Object.values(windowBars).forEach((bar) => {
    ctx.fillRect(bar.x, bar.y, bar.width, 16);
  });

  ctx.restore();
}

draw();

console.log(window.screenX, window.screenY, screen.availWidth, screen.availHeight);
